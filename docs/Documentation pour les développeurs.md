# Documentation pour les développeurs

Zotero est un outil open source, et nous accueillons toute personne qui souhaite y contribuer. Une expérience dans la programmation n'est pas nécessairement requise! Nous indiquons ci-dessous les différents éléments de l'écosystème de Zotero, et nous précisons le degré de difficulté et les compétences nécessaires pour contribuer. Chaque domaine a sa propre page de départ avec plus de détails.

Si vous avez des questions sur le développement pour lesquelles vous ne trouvez pas de réponses ici, ou si vous souhaitez discuter du développement avec les développeurs de Zotero, vous pouvez consulter et publier des messages sur la [liste de diffusion zotero-dev](https://groups.google.com/forum/#!forum/zotero-dev). Veuillez ne pas utiliser cette liste de diffusion pour [obtenir de l'assistance](/support/getting_help). Les questions concernant l'API Zotero doivent toutefois toujours être adressées à zotero-dev.

  

<table>
<tbody>
<tr class="odd">
<td>&lt; 100% 12% &gt;</td>
<td></td>
</tr>
<tr class="even">
<td><a href="dev/Web API">dev/Web API</a></td>
<td></td>
</tr>
<tr class="odd">
<td>Apprenez comment accéder aux bibliothèques Zotero en ligne grâce à l'API Zotero.</td>
<td></td>
</tr>
<tr class="even">
<td>Difficulté</td>
<td>faible à élevée</td>
</tr>
<tr class="odd">
<td>Compétences</td>
<td>HTTP/REST, JSON</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>&lt; 100% 12% &gt;</td>
<td></td>
</tr>
<tr class="even">
<td><a href="dev/Client Coding">dev/Client Coding</a></td>
<td></td>
</tr>
<tr class="odd">
<td>Apprenez à programmer pour le client Zotero.Cette section couvre également les dépôts de code Zotero et le suivi des problèmes, ainsi que des informations pour le développement de modules complémentaires pour Zotero.</td>
<td></td>
</tr>
<tr class="even">
<td>Difficulté</td>
<td>modérée à élevée</td>
</tr>
<tr class="odd">
<td>Compétences</td>
<td>JavaScript, CSS, Technologies Mozilla (XPCOM, XULRunner, XUL, etc.), Git</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>&lt; 100% 12% &gt;</td>
<td></td>
</tr>
<tr class="even">
<td><a href="dev/Translators">Convertisseurs</a></td>
<td></td>
</tr>
<tr class="odd">
<td>Apprenez à développer les convertisseurs Zotero. Ces fichiers JavaScript permettent à Zotero d'enregistrer des documents à partir de pages Web, d'importer et d'exporter des documents dans divers formats de fichier (par exemple BibTeX, RIS, etc.), et de rechercher des documents pour un identifiant donné (par exemple DOI ou PubMed ID). Comme les convertisseurs sont "sandboxed", partagent une structure similaire et sont des morceaux de code relativement courts, ils conviennent parfaitement aux codeurs JavaScript débutants.</td>
<td></td>
</tr>
<tr class="even">
<td>Difficulté</td>
<td>faible à modérée</td>
</tr>
<tr class="odd">
<td>Compétences</td>
<td>JavaScript, HTML/XML, DOM, expressions régulières, XPath</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>&lt; 100% 12% &gt;</td>
<td></td>
</tr>
<tr class="even">
<td><a href="fr/dev/citation_styles">Styles bibliographiques</a></td>
<td></td>
</tr>
<tr class="odd">
<td>Apprenez à créer ou à modifier les styles CSL (Citation Style Language) que Zotero utilise afin de mettre en forme les citations et bibliographies. Dans cette section, nous allons aussi discuter du processeur citeproc-js que Zotero utilise pour traiter les styles CSL, et de citeproc-node, une adaptation de citeproc-js pour une utilisation en ligne.</td>
<td></td>
</tr>
<tr class="even">
<td>Difficulté</td>
<td>faible à modérée</td>
</tr>
<tr class="odd">
<td>Compétences</td>
<td>XML, JavaScript</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>&lt; 100% 12% &gt;</td>
<td></td>
</tr>
<tr class="even">
<td><a href="dev/Documentation">dev/Documentation</a></td>
<td></td>
</tr>
<tr class="odd">
<td>Apprenez comment vous pouvez contribuer à la documentation wiki de Zotero pour assurer qu'elle soit complète, à jour et de qualité. Facilitez l'apprentissage de l'utilisation de Zotero par les utilisateurs, la découverte de nouvelles fonctionnalités et la recherche de solutions aux problèmes, et permettez aux développeurs d'obtenir les informations dont ils ont besoin.</td>
<td></td>
</tr>
<tr class="even">
<td>Difficulté</td>
<td>faible</td>
</tr>
<tr class="odd">
<td>Compétences</td>
<td>écrire et éditer, édition de wiki</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>&lt; 100% 12% &gt;</td>
<td></td>
</tr>
<tr class="even">
<td><a href="dev/Localization">Localisation</a></td>
<td></td>
</tr>
<tr class="odd">
<td>Apprenez comment vous pouvez aider à traduire les différentes parties de l'écosystème Zotero (le client Zotero, la documentation wiki, et les styles bibliographiques CSL) dans d'autres langues.</td>
<td></td>
</tr>
<tr class="even">
<td>Difficulté</td>
<td>faible</td>
</tr>
<tr class="odd">
<td>Compétences</td>
<td>connaissance de l'anglais et d'au moins une autre langue</td>
</tr>
</tbody>
</table>

<table>
<tbody>
<tr class="odd">
<td>&lt; 100% 12% &gt;</td>
<td></td>
</tr>
<tr class="even">
<td>Faites en sorte que votre site web prenne en charge Zotero</td>
<td></td>
</tr>
<tr class="odd">
<td>Vous pouvez rendre votre site accessible à Zotero en utilisant des normes ouvertes. Apprenez comment:<br />
<a href="dev/exposing_metadata">Exposer vos métadonnées</a><br />
<a href="dev/creating_locate_engines_using_opensearch">Créer un moteur de recherche OpenSearch Lookup Engine</a></td>
<td></td>
</tr>
</tbody>
</table>

## Ressources supplémentaires

<table>
<thead>
<tr class="header">
<th><a href="dev/workshops">Developer Workshops</a></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Utilisez cette section du wiki pour chercher ou ajouter des informations sur l'un des ateliers de développement de Zotero.</td>
<td></td>
</tr>
</tbody>
</table>

<table>
<thead>
<tr class="header">
<th><a href="dev/Project Ideas">dev/Project Ideas</a></th>
<th></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>Alors que l'équipe de développement de Zotero est constamment occupée à ajouter de nouvelles fonctionnalités à Zotero, ses ressources sont limitées. Par conséquent, certaines des idées de projets, bien que bonnes, ne reçoivent pas l'attention qu'elles méritent. Si vous cherchez un projet Zotero intéressant sur lequel travailler, regardez ici pour trouver des idées. Dans cette section, nous suivons également l'avancement des projets.</td>
<td></td>
</tr>
</tbody>
</table>
