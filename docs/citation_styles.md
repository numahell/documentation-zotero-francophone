# Les styles bibliographiques

Zotero utilise des styles écrits dans le langage [Citation Style Language](http://citationstyles.org/) (CSL) et le logiciel de traitement CSL [citeproc-js](https://bitbucket.org/fbennett/citeproc-js/wiki/Home) pour [créer des citations et des bibliographies](/support/creating_bibliographies).

Pour plus d'informations sur l'utilisation de styles CSL existants avec Zotero, consultez la page de la documentation générale [Les styles bibliographiques](/support/fr/styles).

## Éditer des styles CSL

Pour des instructions de base sur la façon d'éditer les styles CSL pour Zotero, consultez le [guide pas à pas](fr/dev/citation_styles/style_editing_step-by-step). Vous trouverez plus de documentation à l'adresse <http://citationstyles.org/citation-style-language/documentation/>.

## Correspondance entre les variables et les types de document de Zotero et de CSL

Une correspondance entre les types de document et les variables de Zotero et ceux de CSL est disponible [ici](https://aurimasv.github.io/z2csl/typeMap.xml). Une extension Zotero pour créer et exporter une correspondance à partir d'une installation locale de Zotero est disponible [ici](https://github.com/aurimasv/z2csl). Elle n'est toutefois pas encore compatible avec la version 5 de Zotero, voir [l'issue en cours à ce sujet](https://github.com/aurimasv/z2csl/pull/17).

## Soumettre des styles à l'entrepôt CSL

Les styles bibliographiques peuvent être hébergés dans [l'entrepôt CSL](https://github.com/citation-style-language/styles). Suivez [ces instructions](https://github.com/citation-style-language/styles/blob/master/CONTRIBUTING.md) pour que votre style soit ajouté à l'entrepôt.

## Auto-héberger des styles CSL

Si vous décidez d'héberger en ligne vous-même des styles CSL, indiquer comme type MIME “vnd.citationstyles.style+xml” permet à des applciations comme Zotero de reconnaître et d'installer automatiquement vos styles.

## citeproc-node

[citeproc-node](dev/citation_styles/citeproc-node) est un adapteur de [citeproc-js](https://bitbucket.org/fbennett/citeproc-js/wiki/Home) côté serveur qui permet le rendu des citations et des bibliographies.
