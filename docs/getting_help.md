# Obtenir de l'aide

Besoin d'aide? **Il y a de fortes chances que quelqu'un se soit déjà posé la même question que vous.** Afin de vous aider le plus rapidement possible à résoudre cotre problème, suivez les étapes ci-dessous.

### Étape 1 : Mettez à jour Zotero

[Mettez à jour Zotero](installation) si vous n'utilisez pas la dernière version, car votre problème a peut-être déjà été résolu dans la version la plus récente de Zotero. Vous pouvez vérifier votre version en sélectionnant "À propos de Zotero" dans le menu "Zotero" (Mac) ou dans le menu "Aide" de Zotero (Windows/Linux).

### Étape 2 : Consultez les pages de dépannage

Si vous avez une question générale à propos de Zotero, vérifiez [la foire aux questions](Frequently Asked Questions) et [la base de connaissances](kb).

Si vous rencontrez un problème, les pages de dépannage dédiées suivantes peuvent vous aider.

-   [Problèmes pour installer Zotero](installation)
-   [Problèmes avec vos données Zotero](zotero_data) (par ex., bibliothèque manquante, restauration depuis une sauvegarde)
-   [Problèmes pour enregistrer des documents à partir de sites web](troubleshooting_translator_issues)
-   [Problèmes avec les extensions de traitement de texte](word_processor_plugin_troubleshooting)
-   [Problèmes avec la gestion des fichiers](/kb/file_handling_issues) (par ex., PDF s'ouvrant dans le mauvais logiciel)
-   [Problèmes avec la synchronisation des données](/fr/kb/changes_not_syncing) (les documents de votre bibliothèque Zotero)
-   [Problèmes avec la synchronisation des fichiers](/fr/kb/files_not_syncing) (les fichiers joints aux documents de votre bibliothèque)

### Etape 3 : Publiez un message sur les forums

Si vous ne parvenez pas à trouver la réponse à votre question dans la documentation, publiez un message sur les forums Zotero, où vous obtiendrez un support rapide et expert de la part à la fois des développeurs de Zotero et des membres de la communauté. Consultez la page [Comment fonctionne le support Zotero](zotero_support) pour plus d'informations sur la manière dont cela nous permet d'apporter le meilleur support possible.

En général, veuillez lancer un nouveau fil de discussion pour votre problème. Il est préférable de lancer un nouveau fil de discussion plutôt que d'alimenter une discussion avec un problème qui peut être sans relation avec le sujet discuté. Les développeurs de Zotero ou les bénévoles qui répondent sur les forums peuvent vous indiquer un fil de discussion pertinent s'il en existe un.

Si vous êtes nouveau sur les forums Zotero, veuillez lire les [lignes de conduite sur les forums Zotero](forum guidelines) et les [procédures de signalement des problèmes](reporting problems) avant de publier un message.

**Note:** Vous devez [créer un compte Zotero](/user/register/) et vous identifier pour publier des messages sur les forums Zotero. Vous pouvez choisir un nom d'utilisateur différent pour les forums à partir des [paramètres de votre compte](/settings/account).

<p style="font-size: 14px; font-weight: bold"><a href="https://forums.zotero.org">Aller sur les forums Zotero</a></p>
