### Puis-je empêcher la boîte de dialogue "Ajouter une citation" des modules de traitement de texte de se déplacer derrière la fenêtre du traitement de texte ?

Oui, en modifiant la [préférence cachée](preferences/hidden_preferences#word_processor_plugin) extensions.zotero.integration.keepAddCitationDialogRaised.

![](tag>kb-fr word_processors-fr)
