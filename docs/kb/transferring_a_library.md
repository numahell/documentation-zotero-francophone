#### Comment puis-je transférer ma bibliothèque Zotero vers un autre ordinateur ?

##### Option A: Copier le répertoire de données Zotero

La façon la plus fiable de transférer entièrement votre bibliothèque Zotero vers un autre ordinateur est de copier le [répertoire des données Zotero](fr/zotero_data) depuis votre premier ordinateur vers votre nouvel ordinateur.

Pour localiser vos données Zotero, ouvrez les [préférences](fr/preferences) de Zotero et cliquez sur "Ouvrir le répertoire de données" dans l'onglet Avancées -&gt; Fichiers et dossiers. Reportez-vous à [la rubrique "Emplacements par défaut"](fr/zotero_data#emplacements_par_defaut) pour connaître les emplacements par défaut du répertoire de données.

Assurez-vous d'avoir fermé Zotero sur les deux ordinateurs avant de copier les fichiers Zotero.

En plus de la méthode ci-dessus, vous pouvez aussi [utiliser une clé USB pour synchroniser vos données entre plusieurs ordinateurs](using_multiple_computers).

##### Option B: Activer la synchronisation Zotero

Vous pouvez également [synchroniser](fr/sync) automatiquement votre bibliothèque Zotero entre plusieurs ordinateurs via les serveurs Zotero.

##### Avertissement

Exporter et importer votre bibliothèque (par exemple via Zotero RDF) n'est pas une option recommandée. Aucun des formats d'export disponibles ne permet un transfert complet et sans perte des données de votre bibliothèque. De plus cela brisera les connexions existantes entre votre bibliothèque Zotero et vos fichiers de traitement de texte.

![](tag>kb-fr)
