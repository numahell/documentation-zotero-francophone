# Accueil

![logo de Zotero](./images/zotero.png)

Cette documentation a pour but de vous permettre de découvrir le logiciel Zotero et d'approfondir vos compétences pour l'utiliser plus efficacement,

## Licences

Le contenu est sous licence Creative Commons BY SA (Paternité et partage sous les mêmes conditions) 4.0 International.
Le site, généré par MkDocs est sous licence BSD.

Le code source de la documentation et tous ses contenus sont disponibles 

## Contribuer à la documentation

Si vous souhaitez contribuer à la documentation vous pouvez écrire à l'adresse test@framalistes.org.
