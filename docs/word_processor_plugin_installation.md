# Installer les modules pour logiciel de traitement de texte de Zotero

Les [modules pour logiciel de traitement de texte](word_processor_integration) sont fournis avec Zotero et doivent être installés automatiquement pour chaque [logiciel de traitement de texte pris en charge (page en anglais)](/system_requirements#word_processors) installé sur votre ordinateur lorsque vous démarrez Zotero pour la première fois.

Vous pouvez réinstaller ultérieurement ces modules depuis l'onglet Citer -&gt; Traitements de texte dans les préférences de Zotero. Si vous rencontrez des difficultés, consultez les pages [Installer manuellement les modules pour logiciel de traitement de texte de Zotero](word_processor_plugin_manual_installation) ou [Dépannage des modules pour logiciel de traitement de texte](Word Processor Plugin Troubleshooting).

Si vous avez déjà installé les versions Firefox des modules pour logiciel de de traitement de texte dans Zotero 5.0 ou Zotero Standalone 4.0, vous devez les désinstaller à partir de Outils -&gt; Extensions.
