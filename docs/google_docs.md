# Zotero et Google Documents

Zotero permet d'ajouter des citations et des bibliographies facilement dans les documents Google.

Pour citer des références dans des documents Google, vous devez avoir le connecteur Zotero et le logiciel Zotero doit être ouvert sur votre poste.

Zotero fonctionne aussi avec [Word](word_processor_plugin_usage) et [LibreOffice](libreoffice_writer_plugin_usage).

## Interface

Le connecteur Zotero ajoute un nouveau menu "Zotero" dans Google Documents :

<img src="//google-docs-menu.png" width="400" alt="google-docs-menu.png" />

Un bouton Zotero s'ajoute également à la barre d'outils pour l'ajout facile de citations :

<img src="//google-docs-toolbar.png" width="300" alt="google-docs-toolbar.png" />

Les options suivantes se trouvent dans le menu Zotero menu :

<table>
<tbody>
<tr class="odd">
<td>&lt; 100% 27% &gt;</td>
<td></td>
</tr>
<tr class="even">
<td>Add/Edit Citation</td>
<td>Ajoute une nouvelle citation ou modifie une citation existante dans votre document, à l'endroit où se trouve votre curseur.</td>
</tr>
<tr class="odd">
<td>Add/Edit Bibliography</td>
<td>Insère une bibliographie à l'endroit où se trouve votre curseur ou modifie la bibliographie existante.</td>
</tr>
<tr class="even">
<td>Preferences</td>
<td>Ouvre la fenêtre des préférences, ce qui permet entre autres de changer le style bibliographique.</td>
</tr>
<tr class="odd">
<td>Refresh</td>
<td>Met à jour toutes les citations et la bibliographie, en fonction des modificaionts aux métadonnées modifiées dans la bibliothèque Zotero.</td>
</tr>
<tr class="even">
<td>Unlink Citations</td>
<td>Supprime les codes de champ Zotero du document. Supprimer les codes de champ Zotero empêche toute nouvelle mise à jour des références et bibliographies. Supprimer les codes de champ est <strong>irréversible</strong>, et ne devrait être effectué qu'à la toute fin de votre travail et dans une copie de votre document.</td>
</tr>
</tbody>
</table>
