# Modules pour logiciel de traitement de texte

Parmi les différentes façons de [créer des bibliographies](creating_bibliographies) (ainsi que des citations dans le texte et des notes de bas de page) automatiquement, les modules pour logiciel de traitement de texte, faciles à utiliser, sont les plus puissants. Ces modules, disponibles pour Microsoft Word et LibreOffice, créent des bibliographies dynamiques : insérez une nouvelle citation dans le texte de votre manuscrit, et la bibliographie sera automatiquement mise à jour pour inclure le document cité. Corrigez le titre d'un élément dans votre bibliothèque Zotero, et d'un simple clic le changement sera répercuté dans vos documents.

Consultez les liens suivants pour débuter avec ces modules.

-    [Utiliser le module Zotero pour Word](Word Processor Plugin Usage)

<!-- -->

-    [Utiliser le module Zotero pour LibreOffice](LibreOffice Writer Plugin Usage)

<!-- -->

-    [Utiliser Zotero avec Google Docs](google_docs)

<!-- -->

-    [Dépannage des modules pour logiciel de traitement de texte](Word Processor Plugin Troubleshooting)

[Des extensions tierces](plugins#word_processor_and_writing_integration) sont également disponibles pour intégrer Zotero à d'autres systèmes de traitement de texte et d'écriture.

======= Guide vidéo ========

Vous pouvez également avoir un avant-goût du fonctionnement de ces modules en regardant les vidéocaptures d'écran de [Zotero et Word](http://www.youtube.com/watch?v=RuRF8zxkxIo#!) (par Hannah Rempel, utilisant l'interface de citation rapide) et de [Zotero et OpenOffice](http://www.youtube.com/watch?v=JFkWoBAJY5c)\] (par Ryan Guy, utilisant la fenêtre classique d'ajout de citation). Notez que ces vidéos ont été réalisées à partir d'anciennes versions de Zotero ; les boutons du module Zotero Word ont une apparence légèrement différente dans les versions plus récentes.
